var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

let i = 0;

const getRead = (time) => {
 readBooksPromise(time,books[i]).then(function(sisaWaktu){
    i++;
    if (sisaWaktu !== 0 && i < books.length){
      	getRead(sisaWaktu)
    }
  }).catch(error => console.log(error))
}

getRead(10000);

//Soal 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var saya = pertama.substr(0, 4);
var senang = pertama.substring(12, 18);
var belajar = kedua.substr(0, 7);
var js = kedua.substring(8, 18).toUpperCase();
console.log(saya.concat(' ', senang, ' ', belajar, ' ', js));



//Soal 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angkaPertama = Number(kataPertama);
var angkaKedua = Number(kataKedua);
var angkaKetiga = Number(kataKetiga);
var angkaKeempat = Number(kataKeempat);

console.log(((kataPertama + kataKetiga) * kataKeempat) % kataKedua);



//Soal 3

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15); // do your own! 
var kataKetiga = kalimat.substring(15, 19); // do your own! 
var kataKeempat = kalimat.substring(19, 25); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//Soal 1

function next_date(tanggal, bulan, tahun) {

    if (bulan == 1) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 2;
        } else {
            var newday = tanggal + 1;
            var newbulan = 1;
        }
    } else if (bulan == 2) {
        if (tanggal == 29) {
            var newday = 1;
            var newbulan = 3;
        } else {
            var newday = tanggal + 1;
            var newbulan = 2;
        }
    } else if (bulan == 3) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 4;
        } else {
            var newday = tanggal + 1;
            var newbulan = 3;
        }
    } else if (bulan == 4) {
        if (tanggal == 30) {
            var newday = 1;
            var newbulan = 5;
        } else {
            var newday = tanggal + 1;
            var newbulan = 4;
        }
    } else if (bulan == 5) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 6;
        } else {
            var newday = tanggal + 1;
            var newbulan = 5;
        }
    } else if (bulan == 6) {
        if (tanggal == 30) {
            var newday = 1;
            var newbulan = 7;
        } else {
            var newday = tanggal + 1;
            var newbulan = 6;
        }
    } else if (bulan == 7) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 8;
        } else {
            var newday = tanggal + 1;
            var newbulan = 7;
        }
    } else if (bulan == 8) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 9;
        } else {
            var newday = tanggal + 1;
            var newbulan = 8;
        }
    } else if (bulan == 9) {
        if (tanggal == 30) {
            var newday = 1;
            var newbulan = 10;
        } else {
            var newday = tanggal + 1;
            var newbulan = 9;
        }
    } else if (bulan == 10) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 11;
        } else {
            var newday = tanggal + 1;
            var newbulan = 10;
        }
    } else if (bulan == 11) {
        if (tanggal == 30) {
            var newday = 1;
            var newbulan = 12;
        } else {
            var newday = tanggal + 1;
            var newbulan = 11;
        }
    } else if (bulan == 12) {
        if (tanggal == 31) {
            var newday = 1;
            var newbulan = 1;
            var tahun = tahun + 1;
        } else {
            var newday = tanggal + 1;
            var newbulan = 12;
        }
    }

    switch (newbulan) {
        case 1:
            newbulan = "Januari";
            break;

        case 2:
            newbulan = "Februari";
            break;

        case 3:
            newbulan = "Maret";
            break;

        case 4:
            newbulan = "April";
            break;

        case 5:
            newbulan = "Mei";
            break;

        case 6:
            newbulan = "Juni";
            break;

        case 7:
            newbulan = "Juli";
            break;

        case 8:
            newbulan = "Agustus";
            break;

        case 9:
            bulan = "September";
            break;

        case 10:
            newbulan = "Oktober"
            break;

        case 11:
            newbulan = "November"
            break;

        case 12:
            newbulan = "Desember"
            break;
    }



    var besok = String(newday);
    var tahunBaru = String(tahun);
    return besok + " " + newbulan + " " + tahunBaru;

}


var haribesok = next_date(29, 2, 2020);
console.log(haribesok);




//Soal 2


function jumlah_kata(string) {
    var kata = string.split(" ");
    return kata.length;
}

var jawaban = jumlah_kata("Nama saya Nabila Asmarani Ichsan");
console.log(jawaban);
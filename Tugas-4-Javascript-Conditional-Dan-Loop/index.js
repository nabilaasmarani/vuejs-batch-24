//Soal 1

var nilai = 90;

if (nilai >= 85) {
    console.log("A")
} else if (nilai >= 75 && nilai < 85) {
    console.log("B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("C");
} else if (nilai >= 55 && nilai < 65) {
    console.log("D");
} else {
    console.log("E");
}



//Soal 2

var tanggal = 13;
var bulan = 2;
var tahun = 1997;

switch (bulan) {
    case 1:
        console.log(tanggal, "Januari", tahun);
        break;

    case 2:
        console.log(tanggal, "Februari", tahun);
        break;

    case 3:
        console.log(tanggal, "Maret", tahun);
        break;

    case 4:
        console.log(tanggal, "April", tahun);
        break;

    case 5:
        console.log(tanggal, "Mei", tahun);
        break;

    case 6:
        console.log(tanggal, "Juni", tahun);
        break;

    case 7:
        console.log(tanggal, "Juli", tahun);
        break;

    case 8:
        console.log(tanggal, "Agustus", tahun);
        break;

    case 9:
        console.log(tanggal, "September", tahun);
        break;

    case 10:
        console.log(tanggal, "Oktober", tahun);
        break;

    case 11:
        console.log(tanggal, "November", tahun);
        break;

    case 12:
        console.log(tanggal, "Desember", tahun);
        break;

    default: console.log("Tidak ada tanggal lahir")
        break;
}



//Soal 3

var i = 1
var row = 7
while (i <= row) {
    var pagar = "";
    for (j = 1; j <= i; j++) {
        pagar += "#";
    }
    console.log(pagar);
    i++;
}



//Soal 4

var m = 10;
var output = "";
var iterasiOutput = 1;
for (k = 1; k <= m; k++) {
    switch (iterasiOutput) {
        case 1:
            output = "I love programming";
            break;

        case 2:
            output = "I love Javascript";
            break;

        case 3:
            output = "I love VueJS";
            break;
    }

    if (iterasiOutput == 3) {
        iterasiOutput = 1;
    } else {
        iterasiOutput++;
    }

    if (k % 3 == 0) {
        console.log(k + " - " + output);
        var strip = "";
        for (l = 1; l <= k; l++) {
            strip += "=";
        }
        console.log(strip);
    } else {
        console.log(k + " - " + output);
    }
}

